# Uso del Flujo de Autenticación JWT Bearer Token con Salesforce#

Este proyecto experimenta con el Flujo de Autenticación JWT de Salesforce.
Después de buscar ejemplos y no encontrar un proyecto completo, me decidí a proporcionar este ejemplo completo.

### Objetivo ###
En cualquier compañía, en algún momento se plantea la necesidad de acceder a servicios expuestos en Salesforce, desde sistemas OnPremise o en el Cloud. 

El consumo de estos servicios requiere de un mecanismo de autenticación, para los cuales, la plataforma Salesforce proporcionad diferentes opciones (https://help.salesforce.com/apex/HTViewHelpDoc?id=remoteaccess_authenticate.htm&language=en_US).

En este proyeccto, proporciono un ejemplo de como consumir el JWT Flow, dado que es uno de los más adientes para la integración entre sistemas. 

Destacar que, en el apartado, **Requerimientos**, especifico todo aquellos que se requiere, para que tengas claro todo lo que necesitas.

### Requisitos ###
* Debe crearse una Connected App 
* Debe crearse un certificado y disponer de la Clave Privada (alternativamente modificar el código para obtenerla del certificado)
* Finalmente se construye la petición y se realiza un POST con ciertos parámetros 

### Bibliografía ###
Para entender este mecanismo de autenticación hay que leer con detenimiento la siguiente bibligrafía:

* OAuth 2.0 JWT Bearer Token Flow: https://help.salesforce.com/HTViewHelpDoc?id=remoteaccess_oauth_jwt_flow.htm
* Creating a Connected App: https://help.salesforce.com/apex/HTViewHelpDoc?id=connected_app_create.htm
* Creación de una clave privada y un certificado: http://www.aboveandbeyondcloud.com/jwt-auth/

Además:

* Se requeire un conocimiento básico de cómo realizar una llamada POST HTTP, con cabecereas y cuerpo.
* No es necesario un conocimiento del protocolo Oauth2, aunque es recomendable
 

### Sobre mí ###

* Arquitecto Salesforce en Gas Natural Fenosa donde intento aprender cada día todo lo que puedo, e intentar que nuestros proyectos lleguen a buen puerto. Si te puedo ayudar: [esteve.graells@gmail.com](Link mailto:esteve.graells@gmail.com)