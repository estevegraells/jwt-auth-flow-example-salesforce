/****************************************************************************
 * Copyright (c) 1998-2010 AOL Inc. 
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Adpatado y modificado por Esteve Graells de varias fuentes 
 *  
 ****************************************************************************/

package egraells.integracion.com;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;

import java.net.URL;
import javax.net.ssl.HttpsURLConnection;

import org.apache.commons.codec.binary.Base64;

import java.security.*;
import java.text.MessageFormat;

public class JWTExample {

	public static void main(String[] args) {
		String header = "{\"alg\":\"RS256\"}";
		String claimTemplate = "'{'\"iss\": \"{0}\", \"sub\": \"{1}\", \"aud\": \"{2}\", \"exp\": \"{3}\"'}'";

		StringBuffer token = new StringBuffer();
		
		try {
			
			token.append(Base64.encodeBase64URLSafeString(header.getBytes("UTF-8")));

			// Separate with a period
			token.append(".");

			// Create the JWT Claims Object
			String[] claimArray = new String[4];

			// Valor de Consumer Key obtenido en la creaci�n de la Connected App 
			claimArray[0] = "3MVG9A_f29uWoVQvue2UvqPL_gM6_3Zs.jFBHq1iTC45.Ih.HZ_e6krSct7ybDwwSowOYZyeYTRlPg3F8ZywI";

			// must be the username of the desired Salesforce user - este usuario debe tener el profile definido en la connected App
			claimArray[1] = "esteve.graells@gmail.com";

			// Login URL - https://login.salesforce.com � https://test.salesforce.com
			claimArray[2] = "https://login.salesforce.com";

			// The validity (exp) must be the expiration time of the assertion
			claimArray[3] = Long.toString((System.currentTimeMillis() / 1000) + 300);

			MessageFormat claims;
			claims = new MessageFormat(claimTemplate);
			String payload = claims.format(claimArray);

			// Add the encoded claims object
			String signedPayload = "";

			token.append(Base64.encodeBase64URLSafeString(payload.getBytes("UTF-8")));

			//Lectura de la Clave Privada directamente del fichero para simplificar el c�digo
			//Otros ejemplos pueden leer de un keystore donde se haya introducido el certificado previamente
			
			PrivateKey privateKey = (new PrivateKeyReader("./Certificado/ClavePrivada.key")).getPrivateKey();

			// Sign the JWT Header + "." + JWT Claims Object
			Signature signature = Signature.getInstance("SHA256withRSA");
			signature.initSign(privateKey);
			signature.update(token.toString().getBytes("UTF-8"));
			signedPayload = Base64.encodeBase64URLSafeString(signature.sign());

			// Separate with a period
			token.append(".");

			// Add the encoded signature
			token.append(signedPayload);

			//Crear la petici�n y realizar el POST
			
			//Endpoint destino
			String url = "https://login.salesforce.com/services/oauth2/token";
			URL obj = new URL(url);
			HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();

			//Cabeceras - las m�nimas posibles
			con.setRequestMethod("POST");
			con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");

			//Par�metros requeridos por el token JWT con el token firmado
			String urlParameters = "grant_type=urn:ietf:params:oauth:grant-type:jwt-bearer&assertion=" + token;

			// Envio
			con.setDoOutput(true);
			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			wr.writeBytes(urlParameters);
			wr.flush();
			wr.close();

			//Obtenci�n de resultados y log en la consola
			int responseCode = con.getResponseCode();
			System.out.println("\nSending 'POST' request to URL : " + url);
			System.out.println("Post parameters : " + urlParameters);
			System.out.println("Response Code : " + responseCode);

			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();

			// print result
			System.out.println(response.toString());

			//Esto deber�a ser mucho m�s elaborado ;-)
		} catch (Exception e1) {
			e1.printStackTrace();
		}

	}

}
